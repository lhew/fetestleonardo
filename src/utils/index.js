/**
    Converts a simple array into an array of objects 
    (e.g. ['ford'] becomes [{value: 1, name: 'ford'}])
    containing an value and name attributes
    @param {Array} array a simple array
 */
export const arrayToObj = array => array.map((item, index) => ({value: index + 1, name: item}));

/**
    Returns true if {num} parameter is a valid number (i.e. "2" is not valid)
    @param {any} num the current symbol 
    @returns {Boolean}
 */
export const isValidNum = num => !Number.isNaN(parseInt(num))

/**
    Returns true if {item} is an empty string, null or undefined
    @param {any} item 
    @returns {Boolean}
 */
export const isEmpty = item => item === "" || item === undefined || item === null