import { Field } from 'redux-form';
import React from 'react';

const BaseInput = props => {
    const {
        onChange, 
        disabled, 
        name, 
        label, 
        type,
        wrapperClass,
        placeholder
    } = props;

    return (
        <div className={`input-field ${wrapperClass || ""}` }>
            <label htmlFor={name}>{label}</label>
            <Field
                component="input"
                type={type}
                name={name}
                label={label}
                placeholder={placeholder}
                onChange={onChange}
                disabled={disabled} />
        </div>);
}

export default BaseInput;