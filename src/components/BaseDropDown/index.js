import { Field } from 'redux-form';
import React from 'react';

const BaseDropDown = props => {

    const {
        onChange, 
        disabled, 
        name, 
        label, 
        wrapperClass,
        placeholder,
        options
    } = props;

    return (<div className={`input-field select${wrapperClass || ""}` }>
                <label htmlFor={name}>{label}</label>
                <Field
                    id={`select-field-${name}`}
                    component="select" 
                    name={name}
                    label={label}
                    onChange={onChange}
                    disabled={disabled}>
                        <option defaultValue value="">{placeholder}</option>
                        {options && options.map(({name, value}, index) => (
                            <option key={index} value={value}>{name}</option>
                        ))}
                </Field>
            </div>)
}

export default BaseDropDown;