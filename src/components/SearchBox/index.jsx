import BaseInput from '../BaseInput';
import BaseDropDown from '../BaseDropDown';
import React from 'react';
import {reduxForm, getFormValues} from 'redux-form';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {arrayToObj, isValidNum, isEmpty} from '../../utils';
import {FORM_NAME} from '../../constants';
import {getBrands, getModels} from '../../actions';

const SearchCars = props => {

	const {
		brands, 
		models, 
		selectedBrand,
		getBrands,
		getModels,
		isLoading,
		disableForm,
		asyncError
	} = props;

	const hasBrands = () => brands && brands.length > 0;

	if(!hasBrands() && !asyncError){
		getBrands();
	}

	return (
		<form className="search-box" id="search-box">
			{asyncError && 
			<p className="async-error">
				Oops, looks like a network error has occurred. 
				Try to run the backend service located at package.json.
				For more info, check the project's README.md
			</p>}
			<h3>Buy a Car</h3>
			<hr/>
			<BaseDropDown 
				type="text" 
				label="Brands" 
				placeholder="Select a brand"
				name="brand" 
				onChange={e => getModels(e.target.value)}
				disabled={asyncError || brands.length === 0}
				options={brands} />

			<BaseDropDown 
				type="text" 
				label="Model" 
				placeholder="Select a model"
				name="model"
				disabled={asyncError || selectedBrand === "" || isLoading}
				options={models} />

			<BaseInput 
				type="text" 
				label="Keywords" 
				placeholder="Type some keywords"
				name="keywords"
				disabled={asyncError}
				/>
			<hr/>
			<button disabled={asyncError || disableForm } type="button">Search cars</button>
		</form>
	)
}

const mapStateToProps = state => {
	const formValues    = getFormValues(FORM_NAME)(state);

	const selectedBrand = formValues && isValidNum(formValues.brand)? formValues.brand : "";
	const selectedModel = formValues ? formValues.model : "";
	const keyWords = formValues ? formValues.keywords : "";
	const brands = arrayToObj(state.vehicles.brands);
	const models = state.vehicles.models ? arrayToObj(state.vehicles.models) : [];
	const isLoading = state.vehicles.isLoading;
	const disableForm = isEmpty(selectedBrand) && isEmpty(selectedModel) && isEmpty(keyWords);
	const asyncError = state.vehicles.asyncError;

	return { 
		selectedBrand, 
		selectedModel, 
		brands, 
		models, 
		isLoading, 
		keyWords, 
		disableForm, 
		asyncError
	};
};

const mapDispatchToProps = dispatch => (bindActionCreators({
    getBrands: () => dispatch(getBrands()),
    getModels: (brand) => dispatch(getModels(brand)),
}, dispatch));

export default reduxForm({
	form: FORM_NAME
})(connect(mapStateToProps, mapDispatchToProps)(SearchCars));