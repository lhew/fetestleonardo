export const GM_BRANDS_INDEX      = 2;
export const FORD_BRANDS_INDEX    = 3;
export const FORD_SILVERADO_INDEX = 3;

export const mockedBrands = ["bmw", "fiat", "gm", "ford", "toyota"];
export const mockedModels = [
  {"id": 1, "models": ["BMW1 Series", "BMW2 Series", "BMW3 Series"]},
  {"id": 2, "models": ["Punto", "Palio", "Marea"]},
  {"id": 3, "models": ["Corsa", "Camaro", "Astra", "Silverado"]},
  {"id": 4, "models": ["Fiesta", "F1000", "Mustang GT"]},
  {"id": 5, "models": ["Corolla", "Etios", "Prius"]}
];