//base path constants
export const BASE_URL               = 'http://localhost:3001';
export const BRANDS_PATH            = `${BASE_URL}/brands`;
export const MODELS_PATH            = `${BASE_URL}/models`;

//reducers constants
export const GET_BRANDS_ASYNC_ERROR = 'GET_BRANDS_ASYNC_ERROR';
export const GET_BRANDS_ASYNC       = 'GET_BRANDS_ASYNC';
export const GET_BRANDS             = 'GET_BRANDS';

export const GET_MODELS_ASYNC       = 'GET_MODELS_ASYNC';
export const GET_MODELS_ASYNC_ERROR = 'GET_MODELS_ASYNC_ERROR';
export const GET_MODELS_LOADING     = 'GET_MODELS_LOADING';
export const GET_MODELS             = 'GET_MODELS';

//redux-forms constants
export const FORM_NAME              = 'simple';
