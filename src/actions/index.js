import {GET_BRANDS, GET_MODELS} from '../constants';

export const getBrands = () => ({type: GET_BRANDS});
export const getModels = brand => ({type: GET_MODELS, brand});