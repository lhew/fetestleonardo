import React, { Component } from 'react';
import store from './store';
import {Provider} from 'react-redux';
import './App.css';
import SearchBox from './components/SearchBox';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <SearchBox />
      </Provider>
    );
  }
}

export default App;
