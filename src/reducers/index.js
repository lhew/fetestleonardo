import { combineReducers} from 'redux';
import { reducer as reduxFormReducer } from 'redux-form';
import vehiclesReducer from './vehiclesReducer'

export default combineReducers({
    form: reduxFormReducer,
    vehicles: vehiclesReducer
});