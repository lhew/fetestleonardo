import {
    GET_BRANDS_ASYNC, 
    GET_BRANDS_ASYNC_ERROR,
    GET_MODELS_ASYNC, 
    GET_MODELS_LOADING,
    GET_MODELS_ASYNC_ERROR
    } from '../constants';

const initialState = {
    brands: [],
    models: [],
    isLoading: false,
    asyncError: false
}
const vehiclesReducer = (state = initialState, action) => {

    switch(action.type){
        case GET_BRANDS_ASYNC:
            return {...state, brands: action.value, isLoading: false, asyncError: false};

        case GET_BRANDS_ASYNC_ERROR:
            return {...state, brands: [], isLoading: false, asyncError: true};

        case GET_MODELS_ASYNC:
            return {...state, models: action.value.models, isLoading: false, asyncError: false};

        case GET_MODELS_ASYNC_ERROR:
            return {...state, models: [], isLoading: false, asyncError: true};

        case GET_MODELS_LOADING:
            return {...state, isLoading: true};

        default:
            return state;
    }
}

export default vehiclesReducer;