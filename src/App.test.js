import React from 'react';
import { mount, configure } from 'enzyme';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import Adapter from 'enzyme-adapter-react-16';
import App from './App';
import store from './store';
import {GM_BRANDS_INDEX, FORD_BRANDS_INDEX, FORD_SILVERADO_INDEX, mockedBrands, mockedModels} from './constants/tests'; 

configure({ adapter: new Adapter() });

const mock = new MockAdapter(axios);

mock.onGet(/(brands)/).reply(200, mockedBrands);

mock.onGet(/(models)\/\d/).reply((config) => {
const id = config.url.match(/\d$/)[0];
return [200, mockedModels[id]];
});

describe('App test', ()  => {

	let wrapper;
	let selectBrand;
	let selectModel;
	let keywordsInput;

	it('renders without crashing', () => {
		wrapper = mount(<App />);
		expect(1).toBe(1);
		expect(wrapper.exists()).toBe(true);
	});

	it('expects the brand popup is disabled for the first tick', () => {
		expect(wrapper.find("#select-field-brand").first().prop('disabled')).toBe(true);
	});

	it('expects the models dropdown to be disabled due to absence of filled data', () => {
		expect(wrapper.find("select").at(1).first().prop('disabled')).toBe(true);
		expect(wrapper.find("select").at(1).first().children().length).toBe(1);
	});

	it('expects the `search cars` button to be disabled due to all fields are blank', () => {
		expect(wrapper.find("button").first().prop('disabled')).toBe(true);
	});

	it('expect Brands dropdown to be populated with mockedBrands\'s array length plus empty option (select an option)' , () => {
		wrapper.update();
		const mockedOptions = ["Select an option", ...mockedBrands];
		expect(wrapper.find("select").first().children().length).toBe(mockedOptions.length);
	});

	it('expect to enable click on `Search cars` button' , () => {
		const selectBrand = wrapper.find('select').first();
		selectBrand.simulate('change', {target : { value : GM_BRANDS_INDEX}});
		expect(wrapper.find("button").first().prop('disabled')).toBe(false);
	});

	it('expect Model\'s dropdown length to be the same of GM\'s models + default option' , () => {
		wrapper.update();
		const GM_MODELS = ["Select an option", ...mockedModels[GM_BRANDS_INDEX].models];
		expect(wrapper.find("select").at(1).children().length).toBe(GM_MODELS.length);
	});
});