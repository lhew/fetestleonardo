import axios from 'axios';
import {BRANDS_PATH, MODELS_PATH, } from '../constants';

export const requestBrands = async () => {
    const request = await axios.get(BRANDS_PATH);
    try{
        const response = await request.data;
        return response;
    }catch(e){
        return new Error('Error: ' + e);
    }
}

export const requestModels = async brandId => {
    const request = await axios.get(MODELS_PATH + '/' + brandId);
    const response = await request.data;
    return response;
}