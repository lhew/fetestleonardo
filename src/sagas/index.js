import {takeLatest, put, call, fork} from "redux-saga/effects";
import {requestBrands, requestModels} from '../api';
import {
    GET_BRANDS_ASYNC_ERROR,
    GET_BRANDS_ASYNC, 
    GET_BRANDS, 
    GET_MODELS_ASYNC,
    GET_MODELS_ASYNC_ERROR, 
    GET_MODELS,
    GET_MODELS_LOADING} from '../constants';

function* getBrandsAsync() {
    try{
        const data = yield call(requestBrands);
        yield put({type: GET_BRANDS_ASYNC, value: data});
    }catch(e){
        yield put({type: GET_BRANDS_ASYNC_ERROR});
    }
}

function* getBrands(){ 
    yield takeLatest(GET_BRANDS, getBrandsAsync)
}

function* getModelsAsync(payload) {
    yield put({type: GET_MODELS_LOADING});
    try{
        const data = yield call(requestModels, payload.brand);
        yield put({type: GET_MODELS_ASYNC, value: data});
    }catch(e){
        yield put({type: GET_MODELS_ASYNC_ERROR});
    }
}

function* getModels(brand){
    yield takeLatest(GET_MODELS, getModelsAsync)
}

export default function* root(data) {
    yield [
        fork(getModels),
        fork(getBrands)
    ];
}