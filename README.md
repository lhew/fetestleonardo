
# Frontend Test
Take a look at `docs` folder to see what was asked

## Project build

This assessment were built with an ejected version of **create-react-app** and **json-server** to serve the application's back-end, under node v8.11.4 (currently lts) and npm v5.6.0.


**Important and useful commands**

* After clone this repo, type `npm install` to install all dependencies and `npm start` to run the server on `http://localhost:3000`
* To run the back-end server on port `http://localhost:3000`, type `npm run backend` 
* To run tests, type:`npm run backend` 
* To generate a dependency-graph image, run `npm run dependency-graph`.
***Important:** bear in mind you might have to install [Graphiz](http://www.graphviz.org/) in order to generate the dependency graphs. You can do it easily if you are using linux or macOS via [Brew](https://brew.sh/) by typing `brew install graphviz` on your terminal.*

**Technologies and tools**

The assessment was build using *forward thinking* mindset, where whe separation of concerns, *composition over inheritance* and scalability / maintainability were taken into account. 

To accomplish the premises above, this assessment contains:
* [`redux`](https://redux.js.org/) to help state maintainance over components
* [`redux-saga`](https://redux-saga.js.org/) middleware to keep asynchronous calls apart from actions e/or reducers
* [`redux-form`](https://redux-form.com/) to help traversing data between components and redux's state
